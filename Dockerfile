# build stage
FROM node:14.21.3-alpine3.16 as build-stage
WORKDIR /app
COPY package*.json /app/
RUN apk update && apk add

RUN node `which npm` install -g @vue/cli && node `which npm` install

COPY . /app/

RUN node `which npm` run build

#production stage
FROM nginx:1.17.9-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
COPY --from=build-stage /app/dist /home/admin/web/app/public_html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]


